# How to contribute to the GEDCOM charts project?
## Report bugs
1. Create a new issue
1. Select 'Bug Report' as issue description template
1. Fill out the bug report template in the issue
## Request features, e.g. new chart types
1. Create a new issue
1. Select 'Feature Request' as issue description template
1. Fill out the feature request template in the issue
## Fix bugs
1. Make sure there is a bug report for this bug.
1. Open a merge request
   1. Mention the bug report issue in the description
   1. Add `[WIP]` at the beginning of the title of the merge request if it is still work in progress
   1. Follow the development guidelines
## Develop new features
1. Make sure there is a feature request for this feature.
1. Open a merge request
   1. Mention the feature request issue in the description
   1. Add `[WIP]` at the beginning of the title of the merge request if it is still work in progress
   1. Follow the development guidelines
   
# Development guidelines
## Project structure
* `index` is the entry point:
    * `index.html` contains the web page markup
    * `index.css` contains styling for `index.html`
    * `index.js` handles user interaction with `index.html`
* `file-processing.js` loads the file and passes on the contents to `gedcom-processing.js`
* `gedcom-processing.js` parses the GEDCOM file and sets the person selection list on the chart customization form
* Charts: each chart should have their own `.js` file, that generates the data for the chart 
from the parsed GEDCOM contents, and displays the chart on the webpage.

## Steps for adding a new chart
* name the chart following the pattern of the already existing charts
* in `index.html` add a new radio button for the chart
* in `index.js` call the right method for the new chart type
* implement the chart
    * charts should be self-contained (including styling) displayed in an `<img>` tag, so that they can be easily downloaded
    * charts should be displayed inside the `#chartContainer` element
    * there should be only one chart inside the `#chartContainer` element at one time

## Code quality
* Follow [JavaScript Best Practices](https://www.w3.org/wiki/JavaScript_best_practices)
* Use [Clean Code](https://gist.github.com/wojteklu/73c6914cc446146b8b533c0988cf8d29)
* Write unit tests if possible
* Follow existing patterns for easier understanding
* Don't forget to update documentation

## Structuring commits
Make the life of the reviewer easy to help your merge request get merged sooner!
* One commit should do one thing
    * Break your work into smaller steps that make sense on their own.
    * Keeping feature changes separate from refactorings will ensure that the reviewer is looking at the right thing from the right perspective.
        * Moving and renaming files should always be their own commit. Mixing code changes with moves will mess up the diff.
* One commit should be easy to understand
    * Before you commit, look at the diff: is it easy to grasp what is happening in this commit?
    * Give a meaningful commit message
    * Reference the issue in the commit title
