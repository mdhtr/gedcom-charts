# gedcom-charts
The goal of this project is to be able to make custom family tree charts with data from GEDCOM files.

Try it out on the [GitLab Page](https://mdhtr.gitlab.io/gedcom-charts/)

## test data
The project is tested manually with GEDCOM export from https://www.rootsfinder.com/

## supported GEDCOM version
The RootsFinder Export claims to use version 5.5.1.

## supported browsers
The project uses the latest (ES11) javascript features, so it might not work if your browser is older or it doesn't support the used features.

Tested in Google Chrome Version 85.0.4183.121

## contributing
Feel free to create pull requests to fix bugs or add new diagrams. 

Please read the CONTRIBUTING.md and follow the instructions there.

# credits
The work is based on the following sources:

Parsing GEDCOM
* [gedcom.js](https://github.com/dcapwell/gedcom.js)
* [GEDCOM 5.5.1 revision 2 annotated specification](https://www.gedcom.org/specs/)

Creating diagrams
* [Nicolas Kruchten: Visualizing Family Trees](http://nicolas.kruchten.com/content/2015/08/family-trees/) and its [accompanying code](https://github.com/nicolaskruchten/genealogy/blob/master/lepage_bandet.html)

How to do stuff
* [How to open a file from local disk](https://stackoverflow.com/a/63227449/4456532)
* [How to read a file line by line](https://stackoverflow.com/a/42316936/4456532)
* [How to do a dropdown search field](https://stackoverflow.com/a/59958678/4456532)
* [How to search for one thing but input another in HTML5 datalist](https://www.jotform.com/blog/html5-datalists-what-you-need-to-know-78024/)
* [How to inline CSS in SVG](http://tutorials.jenkov.com/svg/svg-and-css.html)
* [How to traverse a tree breadth-first in javascript](https://www.digitalocean.com/community/tutorials/js-tree-traversal)
* [How to bind data in D3](https://www.tutorialsteacher.com/d3js/data-binding-in-d3js)
* [How to do accent-insensitive string comparison](https://stackoverflow.com/questions/5700636/using-javascript-to-perform-text-matches-with-without-accented-characters)
* [How to limit file types of file input](https://stackoverflow.com/a/23706177/4456532)
* [How to do JavaScript namespacing](https://addyosmani.com/blog/essential-js-namespacing/)
* [How to split JavaScript namespace across multiple files](https://stackoverflow.com/a/5150145/4456532)
* [How to do event listeners properly](https://stackoverflow.com/questions/58341832/event-is-deprecated-what-should-be-used-instead)
* [How to get the data from a HTML Form](https://stackoverflow.com/a/46376650/4456532)
* [How to convert data from a HTML Form to JS object](https://stackoverflow.com/a/55874235/4456532)
* [How to create SVG elements in the DOM](https://webhint.io/docs/user-guide/hints/hint-create-element-svg/)
* [How to author a proper SVG document](https://jwatt.org/svg/authoring/#doctype-declaration)
* [How to convert an inline SVG to an SVG image](https://gist.github.com/vicapow/758fce6aa4c5195d24be)

References
* [JavaScript Best Practices](https://www.w3.org/wiki/JavaScript_best_practices)
* [A series about Event Handlers](https://www.quirksmode.org/js/events_early.html)

Inspirations for family tree charts
* [Nicolas Kruchten: Visualizing Family Trees](http://nicolas.kruchten.com/content/2015/08/family-trees/)
* [Tracing Genealogical Data with TimeNets](http://vis.stanford.edu/files/2010-TimeNets-AVI.pdf)
* [Family Tree Visualization](http://vis.berkeley.edu/courses/cs294-10-sp10/wiki/images/f/f2/Family_Tree_Visualization_-_Final_Paper.pdf)
* [GEPS 030: New Visualization Techniques](https://gramps-project.org/wiki/index.php?title=GEPS_030:_New_Visualization_Techniques)
* [Printable Family Tree Templates](https://www.familytreetemplates.net/)
* [Family Tree Chart Types](https://www.familytreemagazine.com/premium/family-tree-chart-types/)
