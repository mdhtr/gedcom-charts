## Summary
<!-- Describe the requested feature, i.e. what is the feature? who will use it and why? --> 

### Visual summary
<!-- If it is a chart, attach or link to a picture of it. 
If it is a user interface feature, attach a design or describe how it would look like.
If the feature cannot be visualized, you can delete this section. -->

## Iterations and alternatives
<!-- Try to break down the feature into smaller chunks that make sense on their own, and can be released in an iterative fashion.
List all possible alternatives that would be acceptable instead of the complete feature. -->

## Links / references
<!-- Links to existing solutions, articles, anything that would help to implement the feature -->

/label ~feature
