<!---
Please read this!

Before opening a new issue, make sure to search for keywords in the issues and verify the issue you're about to submit isn't a duplicate.

If you are unsure what information to provide in your report, read these guidelines: 
https://www.softwaretestinghelp.com/how-to-write-good-bug-report/
--->

## Summary
<!-- A brief summary of the bug mostly in 60 words or below. 
Make sure your summary is reflecting on what the problem is and where it is. -->

## Description 
<!-- Please use the following three sections to provide a detailed description of the bug -->
### Reproduce steps
<!-- Clearly, mention the steps to reproduce the bug. 
Which version of which browser to use? 
What kind of GEDCOM to use and how that should be acquired?
Where to click and what to input to experience the bug? -->

### Expected result
<!-- How the application should behave on the above-mentioned steps. -->

### Actual result
<!-- What is the actual result of running the above steps i.e. the bug behavior. -->

## Relevant files, screenshots and/or logs
<!-- Files: attach GEDCOM file that can be used to reproduce the problem.
Screenshots: attach screenshots that show what the problem is, if it is a visible problem.
Logs: paste any relevant logs, e.g. from the browser console - please use code blocks (```) for formatting these. -->

## Possible fixes
<!-- If you can, link to the line of code that might be responsible for the problem. 
Feel free to suggest ideas for fixing the problem. -->

/label ~bug
