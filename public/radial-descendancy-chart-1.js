if (!window.mdhtr) {
  window.mdhtr = {};
}
window.mdhtr.radialDescendancyChart1 = window.mdhtr.radialDescendancyChart1 || function() {
  const chartId = 'radial-descendancy-chart-1';

  function personSelected(formData) {
    const personList = window.mdhtr.gedcomContents.getPersonList();
    const familyList = window.mdhtr.gedcomContents.getFamilyList();
    const descendants = collectDescendants(formData, personList, familyList);
    showGraph(descendants);
  }

  function collectDescendants(formData, personList, familyList) {
    const queue = [];
    const tree = {};
    const birthFixingMarkers = {};

    function addSurnameIfNeeded(person, parent) {
      if (Object.keys(tree).length === 0 || // this is the first item
        !equalsInsensitive(person.surname || '', parent?.surname || '')) {
        return person.surname ? ` ${person.surname}` : '';
      }
      return '';
    }

    queue.push({ id: formData.personSelected, parent: tree, level: 0 });

    while (queue.length > 0) {
      const currentQueueItem = queue.shift();
      const currentPerson = personList[currentQueueItem.id];

      const birthYear = extractYear(currentPerson.birthDate || currentPerson.christeningDate);
      const deathYear = extractYear(currentPerson.deathDate);

      if (!formData.displayDeadChildren && deathYear - birthYear < 16) {
        continue;
      }
      if (!formData.displayLivingPeople && currentPerson.isLiving) {
        continue;
      }

      const currentNode = {
        born: birthYear || (currentQueueItem.parent?.born ? currentQueueItem.parent.born + 20 : undefined),
        displayName: `${currentPerson.givenName}${addSurnameIfNeeded(currentPerson, currentQueueItem.parent)}`,
        surname: currentPerson.surname,
        parentNode: currentQueueItem.parent,
        level: currentQueueItem.level
      };

      currentNode.parentNode.children ?
        currentNode.parentNode['children'].push(currentNode) :
        currentNode.parentNode['children'] = [currentNode];

      const childrenIds = [];
      currentPerson.familiesAsASpouse.forEach(family => childrenIds.push(...(familyList[family].children)));
      childrenIds.forEach(childId => queue.push({ id: childId, parent: currentNode, level: currentNode.level + 1 }));

      if (!currentNode.parentNode?.born && currentNode.born) {
        birthFixingMarkers[currentNode.level] = birthFixingMarkers[currentNode.level] || [];
        birthFixingMarkers[currentNode.level].push(currentNode);
      }
    }

    fixBirthDatesForTree(tree, birthFixingMarkers);
    return cleanUpTree(tree);
  }

  function fixBirthDatesForTree(tree, birthFixingMarkers) {
    const levels = Object.keys(birthFixingMarkers);
    let currentLevel = Math.max(...levels);
    if (currentLevel === 0) { // the root person already has a birth date
      return;
    }

    // from top to bottom, fill up birth dates from existing information
    while (currentLevel >= 0) {
      const currentItems = birthFixingMarkers[currentLevel];
      for (const childWithBirthDate of currentItems) {
        if (!childWithBirthDate.parentNode.born) {
          // if there are multiple children of the same parent and we already set the birth of the parent
          // we don't want to set the birth again.
          const earliestChildBirth = findEarliestChildBirth(childWithBirthDate.parentNode);
          childWithBirthDate.parentNode.born = earliestChildBirth - 20;

          // add the newly set birth to the markers
          birthFixingMarkers[childWithBirthDate.parentNode.level] = birthFixingMarkers[childWithBirthDate.parentNode.level] || [];
          birthFixingMarkers[childWithBirthDate.parentNode.level].push(childWithBirthDate.parentNode);
        }
      }
      currentLevel--;
    }

    // if root person didn't have a birth date, there can be full branches without birth date
    // from bottom up, fill up missing birth dates
    const queue = [];
    queue.push(tree.children[0]);

    while (queue.length > 0) {
      const currentNode = queue.shift();
      if (!currentNode.born) {
        currentNode.born = currentNode.parentNode.born + 20;
      }
      currentNode.children?.forEach(child => queue.push(child));
    }
  }

  function findEarliestChildBirth(node) {
    return node?.children?.reduce((min, child) =>
        (child.born !== undefined && min === undefined) || (child.born < min) ? child.born : min,
      node.born);
  }

  function equalsInsensitive(a, b) {
    const a_mod = a.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
    const b_mod = b.normalize('NFD').replace(/[\u0300-\u036f]/g, '').toLowerCase();
    return a_mod === b_mod;
  }

  function extractYear(date) {
    if (date) {
      const regex = /[0-9]{4}/g;
      const match = date.match(regex);
      if (match.length === 1) {
        return parseInt(match[0]);
      }
    }
    return undefined;
  }

  function cleanUpTree(tree) {
    tree = tree.children[0];
    const queue = [];
    queue.push(tree);

    while (queue.length > 0) {
      const currentNode = queue.shift();
      delete currentNode.surname;
      delete currentNode.parentNode;
      delete currentNode.level;
      currentNode.children?.forEach(child => queue.push(child));
    }
    return tree;
  }

// -------------------------------------------------------------------------------------------------------

  function showGraph(root) {
    const chartContainer = document.getElementById('chartContainer');
    const svgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    const svgDefs = document.createElementNS('http://www.w3.org/2000/svg', 'defs');
    svgDefs.innerHTML = svgStyle;
    svgElement.appendChild(svgDefs);

    const yearOffset = root.born;
    const graphTitle = `Descendants of ${root.displayName}`;
    const decadesToDisplay = getDecades(root.born);

    const diameter = decadesToDisplay.length * 70;

    function yearToY(year) {
      return (year - yearOffset) * 3.5;
    }

    function getY(d) {
      return yearToY(d.born);
    }

    const tree = d3.layout.tree()
      .size([360, diameter / 2 - 120])
      .separation(function(a, b) {
        return (a.parent === b.parent ? 1 : 2) / a.depth;
      });

    const diagonal = d3.svg.diagonal.radial()
      .source(function(d) {
        if (d.source.displayName === '')
          return { x: d.target.x, y: getY(d.source) };
        else
          return { x: d.source.x, y: getY(d.source) };
      })
      .target(function(d) {
        return { x: d.target.x, y: getY(d.target) };
      })
      .projection(function(d) {
        return [d.y, d.x / 180 * Math.PI];
      });

    const svg = d3.select(svgElement)
      .attr('version', '1.1')
      .attr('baseProfile', 'full')
      .attr('xmlns', 'http://www.w3.org/2000/svg')
      .attr('width', diameter)
      .attr('height', diameter)
      .append('g')
      .attr('transform', 'translate(' + diameter / 2 + ',' + diameter / 2 + ')');

    const nodes = tree.nodes(root),
      links = tree.links(nodes);

    const link = svg.selectAll('.link')
      .data(links)
      .enter().append('path')
      .attr('class', 'link')
      .attr('d', diagonal);

    const decade = svg.selectAll('.decade')
      .data(decadesToDisplay)
      .enter().append('g')
      .attr('class', 'decade');
    decade.append('circle')
      .attr('r', function(d) {
        return yearToY(d);
      });

    decade.append('text')
      .attr('dy', '.31em')
      .attr('transform', function(d) {
        return 'translate(-10,-' + yearToY(d) + ')';
      })
      .text(function(d) {
        return d;
      });

    svg.selectAll('.title')
      .data([graphTitle])
      .enter().append('text')
      .attr('class', 'title')
      .attr('transform', function(d) {
        return 'translate(-50,20)';
      })
      .text(function(d) {
        return d;
      });

    const node = svg.selectAll('.node')
      .data(nodes)
      .enter().append('g')
      .attr('class', 'node')
      .attr('transform', function(d) {
        return 'rotate(' + (d.x - 90) + ')translate(' + getY(d) + ')';
      });

    const c = d3.scale.category10();
    node.append('circle')
      .attr('r', 4.5)
      .attr('stroke', function(d) {
        return c(d.depth);
      });
    node.append('text')
      .attr('dy', '.31em')
      .attr('text-anchor', function(d) {
        return d.x < 180 ? 'start' : 'end';
      })
      .attr('transform', function(d) {
        return d.x < 180 ? 'translate(8)' : 'rotate(180)translate(-8)';
      })
      .text(function(d) {
        return d.displayName;
      });
    d3.select(self.frameElement).style('height', diameter - 150 + 'px');

    const doctype = '<?xml version="1.0" standalone="no"?>';
    const source = (new XMLSerializer()).serializeToString(svgElement);
    const blob = new Blob([ doctype + source], { type: 'image/svg+xml;charset=utf-8' });
    const url = window.URL.createObjectURL(blob);
    const img = document.createElement('img');
    img.setAttribute('src', url);
    img.setAttribute('id', chartId);
    if (chartContainer.childElementCount === 0 ) {
      chartContainer.append(img);
    }
    else {
      document.getElementById(chartId).replaceWith(img);
    }
  }

  function getDecades(startingDate) {
    const startingDecade = Math.ceil(startingDate / 10) * 10;
    const lastDecade = Math.ceil(new Date().getFullYear() / 10) * 10;
    const decades = [];
    for (let next = startingDecade; next <= lastDecade; next += 10) {
      decades.push(next);
    }
    return decades;
  }

  const svgStyle = '<style><![CDATA[\n' +
    '.node circle, .node rect {\n' +
    '  fill: #fff;\n' +
    '  stroke-width: 1.5px;\n' +
    '}\n' +
    '\n' +
    '.decade circle {\n' +
    '  fill: none;\n' +
    '  stroke: lightgrey;\n' +
    '  stroke-width: 0.5px;\n' +
    '}\n' +
    '\n' +
    '.node {\n' +
    '  font: 10px sans-serif;\n' +
    '}\n' +
    '\n' +
    '.decade {\n' +
    '  font: 8px sans-serif;\n' +
    '}\n' +
    '\n' +
    '.title {\n' +
    '  font: 12px sans-serif;\n' +
    '}\n' +
    '\n' +
    '.link {\n' +
    '  fill: none;\n' +
    '  stroke: #ccc;\n' +
    '  stroke-width: 1.5px;\n' +
    '}\n' +
    '      ]]></style>';

  return {
    personSelected,
    collectDescendants // visible for testing
  };
}();
