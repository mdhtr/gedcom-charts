if (!window.mdhtr) {
  window.mdhtr = {};
}
window.mdhtr.fileProcessing = window.mdhtr.fileProcessing || function() {
  function openFile() {
    document.getElementById('fileInput').click();
  }

  function readFile(e) {
    const file = e.target.files[0];
    if (!file) return;
    const reader = new FileReader();
    reader.onload = function(e) {
      const fileText = e.target.result;
      window.mdhtr.gedcomContents.parseGedcom(fileText);
    };
    reader.readAsText(file);
  }

  return {
    openFile,
    readFile
  };
}();
