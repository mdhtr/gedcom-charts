if (!window.mdhtr) {
  window.mdhtr = {};
}
document.getElementById('openGedcomButton')
  .addEventListener('click', () => window.mdhtr.fileProcessing.openFile());
document.getElementById('fileInput')
  .addEventListener('change', (e) => window.mdhtr.fileProcessing.readFile(e));
document.getElementById('chartConfigurationForm')
  .addEventListener('submit', (e) => window.mdhtr.processFormData(e));

window.mdhtr.processFormData = function(e) {
  e.preventDefault();
  const rawFormData = new FormData(e.target);
  const formData = Object.fromEntries(rawFormData.entries());
  if (formData.chartType === 'radialDescendancyChart1') {
    window.mdhtr.radialDescendancyChart1.personSelected(formData);
  }
};
