if (!window.mdhtr) {
  window.mdhtr = {};
}
window.mdhtr.gedcomContents = window.mdhtr.gedcomContents || function() {
  let personList, familyList;

  function setContents(contents) {
    document.getElementById('contents').innerHTML = contents;
  }

  function parseGedcom(fileText) {
    const parsedGedcom = processFile(fileText);
    personList = createPersonList(parsedGedcom);
    familyList = createFamilyList(parsedGedcom);

    setSearchOptions(personList);
    setContents('GEDCOM processing done. Ready for chart generation.');
  }

// -------------------------------------------------------------------------------------------------------

  function processFile(fileText) {
    const allLines = fileText.split(/\r?\n/);
    const element_top = formatLine(['-1', 'TOP']);
    let lastElement = element_top;

    const totalLines = allLines.length;

    for (let i = 0; i < totalLines; i++) {
      setContents(`processing line ${i + 1} of ${totalLines}`);
      const line = allLines[i];
      const split = line.trim().split(' ');
      if (split.length === 1 && split[0] === '') {
        continue; // empty line
      }
      const element = formatLine(split);
      lastElement = parseLine(element, lastElement);
    }
    element_top.simplify();
    return element_top;
  }

  function formatLine(split) {
    const level = split.shift();
    let tmp = split.shift(),
      id = null,
      tag = null,
      value = null;

    if (tmp.charAt(0) === '@') {
      // line contains an id
      id = tmp;
      tmp = split.shift();
    }

    tag = tmp;
    if (split.length > 0) {
      value = split.join(' ');
      if (value.match(/@[^@]+@/)) {
        // contains a reference...
        // Family Tree Legends seems to put id in value some times, other times it will put it in id location...
        id = value;
        value = null;
      }
    }
    return new Row(level, id, tag, value);
  }

  function parseLine(element, lastElement) {
    let parent_elem = lastElement;
    while (parent_elem.level > element.level - 1) {
      parent_elem = parent_elem.parent;
    }

    const tag = parent_elem[element.tag];
    if (tag instanceof Array) {
      tag.push(element);
    } else if (tag) {
      parent_elem[element.tag] = [tag, element];
    } else {
      parent_elem[element.tag] = element;
    }
    // parent_elem.children.push(element);
    element.parent = parent_elem;
    return element;
  }

  function Row(level, id, tag, value) {
    if (level) this.level = level;
    if (id) this.id = id;
    if (tag) this.tag = tag;
    if (value) this.value = value;
  }

  Row.prototype.simplify = function() {
    delete this.parent;
    delete this.level;
    delete this.tag;
    for (const key in this) {
      const value = this[key];
      if (value instanceof Array) {
        value.map(function(e) {
          if (e.simplify) {
            e.simplify();
          }
        });
      } else {
        if (value.simplify) {
          value.simplify();
        }
      }
    }
    // delete this.simplify();
  };

// -------------------------------------------------------------------------------------------------------
  function createPersonList(parsedGedcom) {
    const results = {};

    for (const person of parsedGedcom.INDI) {
      results[person.id] = {
        id: person.id,
        givenName: selectOneGivenName(person.NAME),
        surname: selectOneSurname(person.NAME),
        birthDate: person.BIRT?.DATE?.value,
        christeningDate: person.CHR?.DATE?.value,
        deathDate: person.DEAT?.DATE?.value,
        isLiving: !person.DEAT,
        familyAsAChild: person.FAMC?.id,
        familiesAsASpouse: unpackIds(person.FAMS)
      };
    }
    return results;
  }

  function createFamilyList(parsedGedcom) {
    const results = {};
    for (const family of parsedGedcom.FAM) {
      results[family.id] = {
        id: family.id,
        husbandId: family.HUSB?.id,
        wifeId: family.WIFE?.id,
        children: unpackIds(family.CHIL)
      };
    }
    return results;
  }

  function selectOneGivenName(element) {
    return selectOneName(element, 0);
  }
  function selectOneSurname(element) {
    return selectOneName(element, 1);
  }

  function selectOneName(element, index) {
    // name values are presented like this: "Anna Catharina /Schmidt/"
    if (!element) {
      return undefined;
    }
    if (element instanceof Array) {
      return element[0].value.split('/')[index].trim();
    }
    return element.value.split('/')[index].trim();
  }

  function unpackIds(objects) {
    if (!objects) {
      return [];
    }
    if (objects.id) {
      return [objects.id];
    }
    const ids = [];
    for (const object of objects) {
      ids.push(object.id);
    }
    return ids;
  }

  function setSearchOptions(searchResults) {
    let options = '';

    for (const person of Object.values(searchResults)) {
      options += `<option value="${person.id}">${person.givenName} ${person.surname}
        (${person.birthDate || person.christeningDate || '?'} - ${person.deathDate || '?'})</option>`;
    }

    document.getElementById('searchNames').innerHTML = options;
  }

  return {
    parseGedcom,
    getPersonList: () => personList,
    getFamilyList: () => familyList
  };
}();
