import '../public/radial-descendancy-chart-1.js';

function treeStatistics(tree) {
  let totalNodeCount = 0,
    hasBirthDateCount = 0,
    hasNoBirthDateCount = 0;

  const queue = [];
  queue.push(tree);
  while (queue.length > 0) {
    const currentQueueItem = queue.shift();
    totalNodeCount++;
    if (currentQueueItem.born) {
      hasBirthDateCount++;
    } else {
      hasNoBirthDateCount++;
    }
    currentQueueItem.children?.forEach(child => queue.push(child));
  }
  return {
    totalNodeCount,
    hasBirthDateCount,
    hasNoBirthDateCount
  };
}

const formData = {
  personSelected: '1',
  displayDeadChildren: true,
  displayLivingPeople: true
};
const personList = {
  '1': {
    givenName: '1',
    familiesAsASpouse: ['1']
  },
  '1.1': {
    givenName: '1.1',
    familiesAsASpouse: ['1.1']
  },
  '1.1.1': {
    givenName: '1.1.1',
    familiesAsASpouse: ['1.1.1']
  },
  '1.1.1.1': {
    givenName: '1.1.1.1',
    familiesAsASpouse: []
  },
  '1.1.1.2': {
    givenName: '1.1.1.2',
    familiesAsASpouse: []
  },
  '1.1.2': {
    givenName: '1.1.2',
    familiesAsASpouse: ['1.1.2']
  },
  '1.1.2.1': {
    givenName: '1.1.2.1',
    familiesAsASpouse: []
  },
  '1.1.2.2': {
    birthDate: '1964 may 1',
    givenName: '1.1.2.2',
    familiesAsASpouse: []
  },
  '1.2': {
    givenName: '1.2',
    familiesAsASpouse: ['1.2']
  },
  '1.2.1': {
    givenName: '1.2.1',
    familiesAsASpouse: ['1.2.1']
  },
  '1.2.1.1': {
    givenName: '1.2.1.1',
    familiesAsASpouse: []
  },
  '1.2.1.2': {
    givenName: '1.2.1.2',
    familiesAsASpouse: []
  },
  '1.2.2': {
    givenName: '1.2.2',
    familiesAsASpouse: ['1.2.2']
  },
  '1.2.2.1': {
    givenName: '1.2.2.1',
    familiesAsASpouse: []
  },
  '1.2.2.2': {
    givenName: '1.2.2.2',
    familiesAsASpouse: []
  }
};
const familyList = {
  '1': { children: ['1.1', '1.2'] },
  '1.1': { children: ['1.1.1', '1.1.2'] },
  '1.1.1': { children: ['1.1.1.1', '1.1.1.2'] },
  '1.1.2': { children: ['1.1.2.1', '1.1.2.2'] },
  '1.2': { children: ['1.2.1', '1.2.2'] },
  '1.2.1': { children: ['1.2.1.1', '1.2.1.2'] },
  '1.2.2': { children: ['1.2.2.1', '1.2.2.2'] }
};

test('collectDescendants fills up missing birth dates', () => {
  const descendants = window.mdhtr.radialDescendancyChart1
    .collectDescendants(formData, personList, familyList);
  const statistics = treeStatistics(descendants);
  expect(statistics.totalNodeCount).toBe(15);
  expect(statistics.hasBirthDateCount).toBe(15);
  expect(statistics.hasNoBirthDateCount).toBe(0);
});
